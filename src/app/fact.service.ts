import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Fact } from './fact-scroller/fact';

@Injectable({
    providedIn: 'root'
})
export class FactService {

    constructor(private http: HttpClient) { }

    getRandomFact(): Observable<Fact> {
        return this.http.get('http://numbersapi.com/random/year?json')
            .pipe(map((x: any) => <Fact>{text: x.text, date: ''+x.number}))
    }
}
