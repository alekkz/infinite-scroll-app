import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { ScrollingModule } from '@angular/cdk/scrolling';
import { FactScrollerComponent } from './fact-scroller/fact-scroller.component';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [
        AppComponent,
        FactScrollerComponent
    ],
    imports: [
        BrowserModule,
        ScrollingModule,
        HttpClientModule

    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
