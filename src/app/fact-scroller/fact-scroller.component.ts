import { Component, OnInit } from '@angular/core';
import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { FactService } from '../fact.service';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { Fact } from './fact';

export class FactsDataSource extends DataSource<Fact> {
    private subscription = new Subscription();
    private cachedFacts = Array.from<Fact>({ length: 0 });
    private dataStream = new BehaviorSubject<Fact[]>(this.cachedFacts);

    constructor(private factService: FactService) {
        super();
        // Start with some data.
        this._fetchFactPage();
    }

    connect(collectionViewer: CollectionViewer): Observable<readonly Fact[]> {
        this.subscription.add(collectionViewer.viewChange.subscribe(range => {
            console.log('--range', range);
            const currentPage = this._getPageForIndex(range.end);
            if (currentPage > this.lastPage) {
              this.lastPage = currentPage;
              this._fetchFactPage();
            }
        }));
        return this.dataStream;
    }

    disconnect(collectionViewer: CollectionViewer) {
        this.subscription.unsubscribe();
    }

    private pageSize = 10;
    private lastPage = 0;

    private _fetchFactPage(): void {
        for (let i = 0; i < this.pageSize; ++i) {
            this.factService.getRandomFact().subscribe(res => {
                this.cachedFacts = this.cachedFacts.concat(res);
                this.dataStream.next(this.cachedFacts);
            });
        }
    }

    private _getPageForIndex(i: number): number {
        return Math.floor(i / this.pageSize);
    }
}

@Component({
    selector: 'app-fact-scroller',
    templateUrl: './fact-scroller.component.html',
    styleUrls: ['./fact-scroller.component.css']
})
export class FactScrollerComponent implements OnInit {

    dataSource: FactsDataSource;

    constructor(private factService: FactService) {
      this.dataSource = new FactsDataSource(factService);
    }

    ngOnInit(): void {
    }

}
